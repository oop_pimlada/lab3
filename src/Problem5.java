import java.util.Scanner;

public class Problem5 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String sentence;

        while (true) {
            System.out.print("Please input: ");
            sentence = sc.nextLine();
            
            if (sentence.trim().equalsIgnoreCase("Bye")) {
                System.out.println("Exit Program");
                break;
            }
            System.out.println(sentence);
        }
        sc.close();
    }
}
